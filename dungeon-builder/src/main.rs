extern crate clap;
use clap::{Arg, ArgMatches, App};
use std::cmp;
use std::fs::DirBuilder;
use std::process::exit;

fn main() {
    let app = new_app();
    let matches = app.get_matches();

    let root = matches.value_of("root").unwrap();
    let maxdepth = max_depth_or_exit(&matches);
    let (rooms, roomcount) = generate_rooms(String::from(root), 1, maxdepth);
    println!("{} rooms", roomcount);
    let dryrun = matches.is_present("dry-run");
    for room in rooms {
        if dryrun {
            println!("{}", &room);
        } else {
            DirBuilder::new().recursive(true).create(room).unwrap();
        }
    }
}

fn generate_rooms(current_room: String, current_depth: u32, max_depth: u32) -> (Vec<String>, u32) {
    let mut rooms: Vec<String> = Vec::new();
    let mut count: u32 = 1;

    // rooms get sparser as we get deeper, also makes sure shallow rooms are populated
    let chance = cmp::min(50, current_depth * 5);
    // roll to see if there are child rooms
    let roll = random_roll(100);
    // if roll less than  or we have reached max depth, then no children
    if roll < chance || current_depth == max_depth {
        rooms.push(current_room);
    } else {
        // determine number of child rooms and create them
        let numrooms = random_roll(4);
        for _r in 1..=numrooms {
            let (children, childcount) = generate_rooms(join_rooms(&current_room, &random_room()), current_depth + 1, max_depth);
            count += childcount;
            for child in children {
                rooms.push(child);
            }
        }
    }
    return (rooms, count);
}

// join to strings with a slash '/'
fn join_rooms(room1: &String, room2: &String) -> String {
    format!("{}/{}", room1, room2)
}

// convert the --max-depth flag into an integer, or exit with an error
fn max_depth_or_exit(matches: &ArgMatches) -> u32 {
    let maxdepth: u32 = match matches.value_of("max-depth").unwrap().parse() {
        Ok(md) => md,
        Err(_) => 0,
    };
    if maxdepth == 0 {
        println!("Error: max depth not a number!");
        exit(1);
    }
    return maxdepth;
}

fn new_app() -> App<'static, 'static> {
    App::new("dungeon-builder")
        .arg(Arg::with_name("max-depth")
             .help("Maximum possible depth")
             .short("m")
             .long("max-depth")
             .value_name("DEPTH")
             .default_value("10")
             .takes_value(true))
        .arg(Arg::with_name("dry-run")
             .help("Print directories instead of creating them")
             .short("d")
             .long("dry-run"))
        .arg(Arg::with_name("root")
             .help("Root directory of dungeon")
             .short("r")
             .long("root")
             .value_name("PATH")
             .takes_value(true)
             .required(true))
}

// return a number in the range 1 to max
fn random_roll(max: u32) -> u32 {
    let r = rand::random::<f32>();
    return ((r * (max - 1) as f32).round() as u32) + 1
}

fn random_room() -> String {
    let size = match random_roll(5) {
        1 => "tiny",
        2 => "small",
        3 => "medium",
        4 => "large",
        5 => "huge",
        _ => "dynamic", // should never see this
    };

    let feature = match random_roll(10) {
        1 => "pristine",
        2 => "clean",
        3 => "dirty",
        4 => "decrepit",
        5 => "dark",
        6 => "well lit",
        7 => "furnished",
        8 => "grand",
        9 => "barren",
        10 => "stripped",
        _ => "cursed", // should never see this
    };

    let shape = match random_roll(8) {
        1 => "circular",
        2 => "oval",
        3 => "triangular",
        4 => "square",
        5 => "rectangular",
        6 => "trapezoidal",
        7 => "hexagonal",
        8 => "octagonal",
        _ => "biangular", // should never see this
    };

    let kind = match random_roll(3) {
        1 => "room",
        2 => "cave",
        3 => "chamber",
        _ => "closet", // should never see this
    };

    let tup = match random_roll(6) {
        1 => (size, feature, shape),
        2 => (size, shape, feature),
        3 => (feature, shape, size),
        4 => (feature, size, shape),
        5 => (shape, feature, size),
        6 => (shape, size, feature),
        _ => ("extra", "special", "secret"), // should never see this
    };
    format!("{} {} {} {}", tup.0, tup.1, tup.2, kind)
}

