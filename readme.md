# container dungeon

[![Docker Repository on Quay](https://quay.io/repository/elmiko/container-dungeon/status "Docker Repository on Quay")](https://quay.io/repository/elmiko/container-dungeon)

a containerized dungeon environment for [rpg-cli](https://github.com/facundoolano/rpg-cli).

## quickstart

The fastest way to try out the container dungeon is to run the pre-built
container, this is the container that is built from every push to
repository. Simply type the following:

```bash
podman run --rm -it quay.io/elmiko/container-dungeon:latest
```

If you are using Docker instead of Podman, simply replace `podman` with `docker`.

## building the dungeon

To build a copy of the image locally, follow these instructions.

**prerequisites**
* podman
* make

type `make all` or `make image` in the root of the project to create a container
named `localhost/container-dungeon`.

to test the container, type `make run`
