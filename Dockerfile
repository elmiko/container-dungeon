FROM registry.fedoraproject.org/fedora as builder

RUN useradd --uid 1001 builder && chown -R 1001:1001 /home/builder
RUN dnf install -y cargo git

USER 1001
WORKDIR /home/builder
ADD --chown=1001:1001 ./_cargo /home/builder/.cargo
ADD --chown=1001:1001 ./registry /home/builder/registry

RUN git clone https://github.com/facundoolano/rpg-cli
WORKDIR /home/builder/rpg-cli
RUN cargo build --release

ADD --chown=1001:1001 ./dungeon-builder /home/builder/dungeon-builder
WORKDIR /home/builder/dungeon-builder
RUN cargo build --release


FROM registry.fedoraproject.org/fedora-minimal

LABEL version="0.2.1"

RUN useradd --home-dir /dungeon --uid 1001 player

COPY --from=builder --chown=root:root /home/builder/rpg-cli/target/release/rpg-cli /usr/bin/rpg-cli
COPY --from=builder --chown=root:root /home/builder/dungeon-builder/target/release/dungeon-builder /usr/bin/dungeon-builder

ADD _bashrc /dungeon/.bashrc

USER 1001
WORKDIR /dungeon
