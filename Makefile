.PHONY: all image run

all: image

image:
	podman build -t localhost/container-dungeon .

run:
	podman run --rm -it localhost/container-dungeon
